class ClientMethods:
    """
    Implements common client methods
    """

    @staticmethod
    def prepare_data(command, uid='', auth='', **values) -> dict:
        """
        Returns a representation of an object
        :param command: str | command to be executed
        :param uid: str | optional | current user
        :param auth: str | optional | authorization of user
        :param values: dict | values to be sent
        :return: dict | the returned object
        """

        response: dict = {
            'command': command,
            'uid': uid,
            'auth': auth,
            'data': values
        }

        return response
