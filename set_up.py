from sqlite3 import OperationalError

from DB import connector


def create_and_populate_db(conn, cur):

    with open('DB/db_dump.sql') as db_dump:

        sql_statements = db_dump.read().split(';')
        for statement in sql_statements:
            try:
                cur.execute(statement)
                if 'INSERT' in statement:
                    conn.commit()
            except (Exception, OperationalError) as e:
                print(f"something went wrong: {e}")


if __name__ == '__main__':
    conn, cur = connector.connect()
    create_and_populate_db(conn, cur)
