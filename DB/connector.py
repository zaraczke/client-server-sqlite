import sqlite3


def connect() -> any:
    """
    Creates and return cursor for data manipulation
    :return: any | cursor
    """
    conn: any = None
    try:
        conn = sqlite3.connect("pythondata.db")
        cursor = conn.cursor()
        return conn, cursor
    except (Exception, sqlite3.DatabaseError) as e:
        print('DB connection error: {}'.format(e))
    finally:
        if conn is None:
            return None


if __name__ == '__main__':
    try:
        connection, cur = connect()
        cur.execute('SELECT "abc"')
        printer = cur.fetchall()
        print(printer)
    except Exception as err:
        print(err)
    finally:
        if connection:
            cur.close()
            connection.close()
