@echo off
echo Setting things up...
move "pythondata.db" "DB/pythondata.db"
python set_up.py

echo Starting unit tests...
python -m unittest discover -s tests/unit -t tests/unit
echo Unit tests done!

echo Starting Integration tests...
python -m unittest discover -s tests/integration -t tests/integration
echo Integration tests done!

echo Cleaning up...
del pythondata.db